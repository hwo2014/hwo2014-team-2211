#include "track.h"

using namespace std;

track::track(const jsoncons::json& data,
             track_piece_factory& track_piece_f,
             track_lane_factory& track_lane_f)
  : _id(data["id"].as<string>()),
    _name(data["name"].as<string>()),
    _pieces(track_piece_f.create_track_piece_vector(data["pieces"])),
    _lanes(track_lane_f.create_track_lane_vector(data["lanes"]))
{
}

string track::get_id() const
{
  return _id;
}

string track::get_name() const
{
  return _name;
}

const track_piece::track_piece_vector& track::get_pieces() const
{
  return _pieces;
}

const track_lane::track_lane_vector& track::get_lanes() const
{
  return _lanes;
}

