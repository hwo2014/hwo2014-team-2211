#include "race.h"
#include <stdexcept>

using namespace std;

race::race(const jsoncons::json& data, track_factory& track_f, car_factory& car_f)
  : _laps(data["raceSession"].get("laps", -1).as<int>()),
    _max_lap_time_ms(data["raceSession"].get("maxLapTimeMs", -1).as<int>()),
    _quick_race(data["raceSession"].get("quickRace", false).as<bool>()),
    _track(track_f.create_track(data["track"])),
    _cars(car_f.create_car_vector(data["cars"])),
    _millis(0)
{
}

int race::get_laps() const
{
  return _laps;
}

int race::get_max_lap_time_ms() const
{
  return _max_lap_time_ms;
}

bool race::is_quick_race() const
{
  return _quick_race;
}

const track& race::get_track() const
{
  return _track;
}

const car::car_vector& race::get_cars() const
{
  return _cars;
}

car& race::get_car_by_color(std::string color) const
{
  for (car::car_vector::iterator itr = _cars.begin(); itr != _cars.end(); itr++)
  {
    if (itr->get_color() == color)
    {
      return *itr;
    }
  }
  throw std::invalid_argument("color");
}

long race::get_millis() const
{
  return _millis;
}

void race::set_millis(long millis)
{
  _millis = millis;
}

