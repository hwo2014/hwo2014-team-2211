#include "track_factory.h"
#include "track.h"

using namespace std;

track_factory::track_factory(
    track_piece_factory& track_piece_f,
    track_lane_factory& track_lane_f)
  : _track_piece_f(track_piece_f),
    _track_lane_f(track_lane_f)
{
}

track& track_factory::create_track(const jsoncons::json& data)
{
  _tracks.push_back(new track(data, _track_piece_f, _track_lane_f));
  return _tracks.back();
}

