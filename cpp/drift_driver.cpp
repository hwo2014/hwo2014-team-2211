#include <cmath>
#include <iostream>
#include "driver.h"
#include "protocol.h"
#include "drift_driver.h"

driver* create_driver(game& g)
{
  return new drift_driver(g);
}

drift_driver::drift_driver(game& g)
  : base_driver(g),
    _throttle(0)
{
}

drift_driver::~drift_driver()
{
}

void drift_driver::analyze_track()
{
  base_driver::analyze_track();
}

jsoncons::json drift_driver::process_tick(long tick)
{
  base_driver::process_tick(tick);

  double look_ahead_distance = LOOK_AHEAD_TICKS * get_distance_per_tick();
  bool throttle_changed = update_throttle(calculate_desired_throttle(look_ahead_distance));

  if (tick % 10 == 0 || throttle_changed)
  {
    print_status(tick, look_ahead_distance);
  }

  if (throttle_changed)
  {
    return hwo_protocol::make_throttle(_throttle);
  }
  else
  {
    return hwo_protocol::make_ping();
  }
}

void drift_driver::print_status(long tick, double look_ahead_distance)
{
  car& car = get_game().get_my_car();
  const track_piece& piece = get_game().get_race().get_track().get_pieces()[car.get_piece_index()];

  // Track info.
  std::cout << "[T" << tick
            << " L" << car.get_lap()
            << " P" << car.get_piece_index();
  if (piece.get_radius() > 0)
  {
    std::cout << (piece.get_angle() > 0 ? " /" : " \\") << piece.get_length()
              << "r" << piece.get_radius();
  }
  else
  {
    std::cout << " |" << piece.get_length();
  }
  std::cout << "]\t";

  if (car.is_crashed())
  {
    std::cout << "[--crashed--]";
  }
  else
  {
    // Speed info
    std::cout << "[speed:" << std::setprecision(3) << get_distance_per_tick()
              << " throt:" << std::setprecision(3) << _throttle
              << " look:" << look_ahead_distance << "]";
  }
  std::cout << std::endl;
}

bool drift_driver::update_throttle(double desired_throttle)
{
  if (get_game().get_my_car().is_crashed())
  {
    // Crashing resets throttle to zero.
    desired_throttle = 0;
  }

  // Ensure the throttle isn't adjusting too much in one tick.
  if (desired_throttle > _throttle + MAX_ACCELERATION)
  {
    desired_throttle = _throttle + MAX_ACCELERATION;
  }
  else if (desired_throttle < _throttle - MAX_DECELERATION)
  {
    desired_throttle = _throttle - MAX_DECELERATION;
  }

  if (_throttle == desired_throttle)
  {
    return false;
  }
  else
  {
    _throttle = desired_throttle;
    return true;
  }
}

double drift_driver::calculate_desired_throttle(double look_ahead_distance)
{
  const track& track = get_game().get_race().get_track();
  const car& my_car = get_game().get_my_car();

  return calculate_desired_throttle(
      track,
      my_car.get_piece_index(),
      my_car.get_in_piece_distance(),
      look_ahead_distance);
}

double drift_driver::calculate_desired_throttle(
    const track& track,
    int start_piece_index,
    double start_piece_distance,
    double look_ahead_distance)
{
  double max_throttle = MAX_THROTTLE;

  // Check each piece between us and the look ahead point.
  // Take the lowest throttle setting we find.
  // This keeps us from missing a curve in between us and the look ahead point.
  int look_ahead_index = drift_driver::get_piece_index(
      track, start_piece_index, start_piece_distance, look_ahead_distance);
  for (int i = look_ahead_index; i >= start_piece_index; i--)
  {
    max_throttle = std::fmin(get_max_throttle(track.get_pieces()[i].get_radius()), max_throttle);
  }

  return max_throttle;
}

double drift_driver::get_max_throttle(double curve_radius)
{
  if (curve_radius <= 0 || curve_radius >= MAX_SPEED_RADIUS)
  {
    // Max throttle.
    return MAX_THROTTLE;
  }
  else
  {
    double factor = curve_radius / MAX_SPEED_RADIUS;
    double spread = MAX_THROTTLE - MIN_CURVE_THROTTLE;
    return MIN_CURVE_THROTTLE + (factor * spread);
  }
}

