#ifndef HWO_TRACK_LANE_FACTORY_H
#define HWO_TRACK_LANE_FACTORY_H

#include <boost/ptr_container/ptr_vector.hpp>
#include <iostream>
#include <jsoncons/json.hpp>
#include "track_lane.h"

class track_lane_factory
{
public:
  track_lane_factory();

  track_lane& create_track_lane(const jsoncons::json& data);
  track_lane::track_lane_vector& create_track_lane_vector(const jsoncons::json& data);

private:
  boost::ptr_vector<track_lane> _track_lanes;
  boost::ptr_vector<track_lane::track_lane_vector> _track_lane_vectors;
};

#endif
