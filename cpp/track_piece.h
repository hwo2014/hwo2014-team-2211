#ifndef HWO_TRACK_PIECE_H
#define HWO_TRACK_PIECE_H

#include <iostream>
#include <vector>
#include <jsoncons/json.hpp>

/**
 * Represents a piece of the race track.
 */
class track_piece
{
public:
  typedef std::vector<track_piece> track_piece_vector;

  track_piece(const jsoncons::json& data);

  double get_length() const;
  double get_radius() const;
  double get_angle() const;
  bool is_switch() const;

private:
  double _length;
  double _radius;
  double _angle;
  bool _switch;
};

#endif
