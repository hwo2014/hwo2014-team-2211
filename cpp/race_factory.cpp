#include "race_factory.h"
#include "car.h"

race_factory::race_factory(track_factory& track_f, car_factory& car_f)
  : _track_f(track_f),
    _car_f(car_f)
{
}

race& race_factory::create_race(const jsoncons::json& data)
{
  _races.push_back(new race(data, _track_f, _car_f));
  return _races.back();
}

