#ifndef HWO_TRACK_PIECE_FACTORY_H
#define HWO_TRACK_PIECE_FACTORY_H

#include <boost/ptr_container/ptr_vector.hpp>
#include <iostream>
#include <jsoncons/json.hpp>
#include "track_piece.h"

class track_piece_factory
{
public:
  track_piece_factory();

  track_piece& create_track_piece(const jsoncons::json& data);
  track_piece::track_piece_vector& create_track_piece_vector(const jsoncons::json& data);

private:
  boost::ptr_vector<track_piece> _track_pieces;
  boost::ptr_vector<track_piece::track_piece_vector> _track_piece_vectors;
};

#endif
