#include "protocol.h"

namespace hwo_protocol
{

  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data)
  {
    jsoncons::json r;
    r["msgType"] = msg_type;
    r["data"] = data;
    return r;
  }

  jsoncons::json make_join(const std::string& name, const std::string& key)
  {
    jsoncons::json data;
    data["name"] = name;
    data["key"] = key;
    return make_request("join", data);
  }
    
  jsoncons::json make_test_join(const std::string& name,
                                const std::string& key,
                                const std::string& trackname,
                                int carcount)
  {
    jsoncons::json data, data_sub_str;
    data_sub_str["name"] = name;
    data_sub_str["key"]  = key;
    data["botId"] = data_sub_str;
    data["trackName"] = trackname;
    data["carCount"] = (carcount > 0 ? carcount : 1);
    return make_request("joinRace", data);
  }

  jsoncons::json make_ping()
  {
    return make_request("ping", jsoncons::null_type());
  }

  jsoncons::json make_throttle(double throttle)
  {
    return make_request("throttle", throttle);
  }
    
  jsoncons::json make_turbo()
  {
    return make_request("turbo", "NyanCat Meow...Meow...Meow");
  }
    
  jsoncons::json make_switch_left()
  {
    return make_request("switchLane", "Left");
  }
  
  jsoncons::json make_switch_right()
  {
    return make_request("switchLane", "Right");
  }

}  // namespace hwo_protocol
