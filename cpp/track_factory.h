#ifndef HWO_TRACK_FACTORY_H
#define HWO_TRACK_FACTORY_H

#include <boost/ptr_container/ptr_vector.hpp>
#include <iostream>
#include <jsoncons/json.hpp>
#include "track.h"
#include "track_piece_factory.h"
#include "track_lane_factory.h"

class track_factory
{
public:
  track_factory(track_piece_factory& track_piece_f, track_lane_factory& track_lane_f);

  track& create_track(const jsoncons::json& data);

private:
  boost::ptr_vector<track> _tracks;
  track_piece_factory& _track_piece_f;
  track_lane_factory& _track_lane_f;
};

#endif
