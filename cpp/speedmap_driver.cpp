#include <cmath>
#include <iostream>
#include "driver.h"
#include "protocol.h"
#include "speedmap_driver.h"

driver* create_driver(game& g)
{
  return new speedmap_driver(g);
}

speedmap_driver::speedmap_driver(game& g)
  : base_driver(g),
    _speed_points(),
    _throttle(0)
{
}

speedmap_driver::~speedmap_driver()
{
}

void speedmap_driver::analyze_track()
{
  base_driver::analyze_track();
  create_speedmap();
}

jsoncons::json speedmap_driver::process_tick(long tick)
{
  base_driver::process_tick(tick);

  double current_speed = get_distance_per_tick();
  double target_speed = get_target_speed();
  bool throttle_changed = update_throttle(current_speed, target_speed);

  if (tick % 10 == 0 || throttle_changed)
  {
    print_status(tick, current_speed, target_speed);
  }

  if (throttle_changed)
  {
    return hwo_protocol::make_throttle(_throttle);
  }
  else
  {
    return hwo_protocol::make_ping();
  }
}

void speedmap_driver::print_status(long tick, double current_speed, double target_speed)
{
  car& car = get_game().get_my_car();
  const track_piece& piece = get_game().get_race().get_track().get_pieces()[car.get_piece_index()];

  // Track info.
  std::cout << "[T" << tick
            << " L" << car.get_lap()
            << " P" << car.get_piece_index() << ":" << car.get_in_piece_distance();
  if (piece.get_radius() > 0)
  {
    std::cout << (piece.get_angle() > 0 ? " /" : " \\") << piece.get_length()
              << "r" << piece.get_radius();
  }
  else
  {
    std::cout << " |" << piece.get_length();
  }
  std::cout << "]\t";

  if (car.is_crashed())
  {
    std::cout << "[--crashed--]";
  }
  else
  {
    // Speed info
    std::cout << "[tspd:" << std::setprecision(3) << target_speed
              << " spd:" << std::setprecision(3) << current_speed
              << " drft:" << std::setprecision(3) << car.get_angle()
              << " throt:" << std::setprecision(3) << _throttle
              << "]";
  }
  std::cout << std::endl;
}

bool speedmap_driver::update_throttle(double current_speed, double desired_speed)
{
  if (get_game().get_my_car().is_crashed())
  {
    // Crashing resets speed/throttle to zero.
    current_speed = 0;
    desired_speed = 0;
  }

  double delta = desired_speed - current_speed;
  double desired_throttle = _throttle + (delta * SPEED_THROTTLE_RATIO);

  desired_throttle = std::fmin(desired_throttle, MAX_THROTTLE);
  desired_throttle = std::fmax(desired_throttle, MIN_THROTTLE);

  if (_throttle == desired_throttle)
  {
    return false;
  }
  else
  {
    _throttle = desired_throttle;
    return true;
  }
}

double speedmap_driver::get_target_speed()
{
  car& my_car = get_game().get_my_car();

  return get_target_speed(
      my_car.get_piece_index(),
      my_car.get_in_piece_distance());
}

double speedmap_driver::get_target_speed(int piece_index, double position)
{
  double speed = -1.0;

  speed_point_vector piece_points = _speed_points[piece_index];
  for (int i = 0; i < piece_points.size(); i++)
  {
    speed_point* p = &piece_points[i];
    speed_point* next = ((i + 1) < piece_points.size() ? &piece_points[i+1] : NULL);
    if (position >= p->get_piece_position() &&
        (next == NULL || position < next->get_piece_position()))
    {
      speed = p->get_target_speed();
    }
  }

  return speed;
}

void speedmap_driver::create_speedmap()
{
  _speed_points.clear();

  const track& trck = get_game().get_race().get_track();
  double last_speed = 0;
  double target_speed = 0;

  for (int piece_index = 0; piece_index < trck.get_pieces().size(); piece_index++)
  {
    _speed_points.insert(piece_index, new speed_point_vector());

    const track_piece& piece = trck.get_pieces()[piece_index];

    for (double position = 0; position < piece.get_length(); position += 1.0)
    {
      target_speed = calculate_target_speed(trck, piece, piece_index, position);
      if (position == 0 || target_speed != last_speed)
      {
        _speed_points[piece_index].push_back(
            new speed_point(piece_index, position, target_speed));

        last_speed = target_speed;
      }
    }
  }
}

/**
 * Target speed including taking into account what is ahead on the track.
 */
double speedmap_driver::calculate_target_speed(
    const track& trck, const track_piece& piece, int piece_index, double position)
{
  const car& my_car = get_game().get_my_car();
  double speed;

  // Get information about the next curve ahead.
  double curve_start = 0;
  double curve_end = 0;
  double curve_total_angle = 0;
  double curve_radius = 0;
  int i = piece_index;
  while (curve_end == 0)
  {
    const track_piece& p = trck.get_pieces()[i];
    if (p.get_radius() > 0)
    {
      if (curve_radius == 0)
      {
        // Started the curve
        curve_start = calculate_distance(trck, piece_index, position, i, 0);
        curve_total_angle = p.get_angle();
        curve_radius = p.get_radius();
      }
      else
      {
        // Continuing the curve.
        curve_total_angle += p.get_angle();
        curve_radius = std::fmin(curve_radius, p.get_radius());
      }
    }
    else if (p.get_length() > MIN_STRAIGHTAWAY_LENGTH && curve_radius > 0)
    {
      // Ended the curve
      curve_end = calculate_distance(trck, piece_index, position, i, 0);
    }

    // Increment i and loop back to beginning of track if necessary.
    i = (i + 1 == trck.get_pieces().size() ? 0 : i + 1);
  }

  if (curve_start > BRAKE_TICKS)
  {
    // We're on a straightaway and don't have to brake yet.
    speed = MAX_SPEED;
  }
  else if (curve_total_angle < CURVE_EXIT_ANGLE)
  {
    // We're exiting a curve and need to start accelerating.
    speed = (MAX_CURVE_EXIT_DRIFT - my_car.get_angle()) * DRIFT_SPEED_RATIO;
  }
  else
  {
    // We're in a curve and need to watch our speed.
    speed = (MAX_DRIFT - my_car.get_angle()) * DRIFT_SPEED_RATIO;
  }

  return speed;
}

/**
 * Max speed for this position in the track, regardless of any look ahead.
 */
double speedmap_driver::calculate_max_speed(
    const track& trck, const track_piece& piece, int piece_index, double position)
{
  double max_speed;

  double radius = piece.get_radius();

  if (radius == 0 || radius > FULL_THROTTLE_RADIUS)
  {
    max_speed = MAX_SPEED;
  }
  else
  {
    max_speed = (radius / FULL_THROTTLE_RADIUS) * MAX_SPEED;
  }

  return max_speed;
}

