#include "gtest/gtest.h"
#include "track_lane.h"
#include "track_lane_factory.h"

namespace {

class track_lane_test : public ::testing::Test {
protected:
  track_lane_test()
    : factory(),
      track_lanes(factory.create_track_lane_vector(jsoncons::json::parse_string(
        "[ \n"
        "  { \n"
        "    \"distanceFromCenter\": -20, \n"
        "    \"index\": 0 \n"
        "  }, \n"
        "  { \n"
        "    \"distanceFromCenter\": 0, \n"
        "    \"index\": 1 \n"
        "  }, \n"
        "  { \n"
        "    \"distanceFromCenter\": 20, \n"
        "    \"index\": 2 \n"
        "  } \n"
        "] \n"
      )))
  {
  }

  virtual ~track_lane_test()
  {
  }

  track_lane_factory factory;
  track_lane::track_lane_vector& track_lanes;
};

TEST_F(track_lane_test, has_three_tracks)
{
  EXPECT_EQ(3, track_lanes.size());
}

TEST_F(track_lane_test, has_correct_values)
{
  EXPECT_EQ(0, track_lanes[0].get_index())
    << "lane 0, index incorrect";

  EXPECT_EQ(-20.0, track_lanes[0].get_distance_from_center())
    << "lane 0, distance_from_center incorrect";

  EXPECT_EQ(1, track_lanes[1].get_index())
    << "lane 1, index incorrect";

  EXPECT_EQ(0.0, track_lanes[1].get_distance_from_center())
    << "lane 1, distance_from_center incorrect";

  EXPECT_EQ(2, track_lanes[2].get_index())
    << "lane 2, index incorrect";

  EXPECT_EQ(20.0, track_lanes[2].get_distance_from_center())
    << "lane 2, distance_from_center incorrect";
}

} // namespace

