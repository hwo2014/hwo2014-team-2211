#ifndef HWO_DRIFT_DRIVER_H
#define HWO_DRIFT_DRIVER_H

#include "base_driver.h"

// The amount of time the car should look ahead to calculate throttle.
static const int LOOK_AHEAD_TICKS = 10;

// The radius at which the car can be at full speed without crashing.
static const double MAX_SPEED_RADIUS = 170;

// The minimum throttle we want to use in a curve.
static const double MIN_CURVE_THROTTLE = 0.0;

// The maximum throttle acceleration per tick.
static const double MAX_ACCELERATION = 0.25;

// The maximum throttle deceleration per tick.
static const double MAX_DECELERATION = 0.25;

class drift_driver : public base_driver
{
public:

  drift_driver(game& g);
  virtual ~drift_driver();

  virtual void analyze_track();
  virtual jsoncons::json process_tick(long tick);

  void print_status(long tick, double look_ahead_distance);
  bool update_throttle(double desired_throttle);
  double calculate_desired_throttle(double look_ahead_distance);
  double calculate_desired_throttle(
      const track& track,
      int start_piece_index,
      double start_piece_distance,
      double look_ahead_distance);
  double get_max_throttle(double curve_radius);

private:
  double _throttle;
};

#endif
