#ifndef HWO_GAME_H
#define HWO_GAME_H

#include <time.h>
#include <string>
#include <iostream>
#include <jsoncons/json.hpp>
#include "car_factory.h"
#include "race_factory.h"
#include "track_factory.h"
#include "track_lane_factory.h"
#include "track_piece_factory.h"

class game
{
public:
  game(race_factory& race_f);

  std::string get_my_car_name() const ;
  std::string get_my_car_color() const ;
  bool is_initialized() const;
  car& get_my_car() const ;
  race& get_race() const ;
  time_t get_start_time() const ;
  time_t get_end_time() const ;
  const jsoncons::json& get_race_results() const ;
  const jsoncons::json& get_best_laps() const ;
  long get_turbo_duration_ticks() const ;
  double get_turbo_factor() const ;

  void clear_turbo();

  void set_your_car_data(const jsoncons::json& data);
  void set_game_init_data(const jsoncons::json& data);
  void set_game_start_data(const jsoncons::json& data);
  void set_car_positions_data(const jsoncons::json& data);
  void set_turbo_available_data(const jsoncons::json& data);
  void set_game_end_data(const jsoncons::json& data);
  void set_tournament_end_data(const jsoncons::json& data);
  void set_crash_data(const jsoncons::json& data);
  void set_spawn_data(const jsoncons::json& data);
  void set_lap_finished_data(const jsoncons::json& data);
  void set_dnf_data(const jsoncons::json& data);
  void set_finish_data(const jsoncons::json& data);

private:
  race_factory& _race_f;

  std::string _my_car_name;
  std::string _my_car_color;
  race* _race;           // todo KK
  time_t _start_time;
  time_t _end_time;
  jsoncons::json _race_results;
  jsoncons::json _best_laps;
  long _turbo_duration_ticks;
  double _turbo_factor;
};

#endif
