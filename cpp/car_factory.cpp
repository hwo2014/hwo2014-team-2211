#include "car_factory.h"
#include "car.h"

using namespace std;

car_factory::car_factory()
{
}

car& car_factory::create_car(const jsoncons::json& data)
{
  _cars.push_back(new car(data));
  return _cars.back();
}

car::car_vector& car_factory::create_car_vector(const jsoncons::json& data)
{
  car::car_vector* cars = new car::car_vector();

  for (size_t i = 0; i < data.size(); ++i)
  {
    cars->push_back(create_car(data[i]));
  }

  _car_vectors.push_back(cars);
  return _car_vectors.back();
}

