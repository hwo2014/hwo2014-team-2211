#ifndef HWO_CAR_H
#define HWO_CAR_H

#include <string>
#include <vector>
#include <iostream>
#include <jsoncons/json.hpp>

/**
 * Represents a car that is on the race track.
 */
class car
{
public:
  typedef std::vector<car> car_vector;

  car(const jsoncons::json& data);

  std::string get_name() const;
  std::string get_color() const;
  double get_angle() const;
  int get_piece_index() const;
  double get_in_piece_distance() const;
  int get_start_lane_index() const;
  int get_end_lane_index() const;
  int get_lap() const;
  bool is_crashed() const;
  int get_crash_count() const;
  int get_lap_time_ticks(int lap) const;
  int get_lap_time_millis(int lap) const;
  bool is_disqualified() const;
  std::string get_dnf_reason() const;
  bool is_finished() const;

  void set_position(const jsoncons::json& data);
  void set_crashed(bool crashed);
  void set_disqualified(bool disqualified, std::string reason);
  void add_lap(long ticks, long millis);
  void update_ranking(int overall, int fastest_lap);
  void set_finished(bool finished);

private:
  typedef std::tuple<long, long> tick_milli;
  typedef std::vector<tick_milli> tick_milli_vector;

  std::string _name;
  std::string _color;
  double _length;
  double _width;
  double _guide_flag_position;
  double _angle;
  int _piece_index;
  double _in_piece_distance;
  int _start_lane_index;
  int _end_lane_index;
  int _lap;
  bool _crashed;
  int _crash_count;
  tick_milli_vector _lap_times;
  int _ranking_overall;
  int _ranking_fastest_lap;
  std::string _dnf_reason;
  bool _finished;
};

#endif
