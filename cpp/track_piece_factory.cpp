#include "track_piece_factory.h"
#include "track_piece.h"

using namespace std;

track_piece_factory::track_piece_factory()
{
}

track_piece& track_piece_factory::create_track_piece(const jsoncons::json& data)
{
  _track_pieces.push_back(new track_piece(data));
  return _track_pieces.back();
}

track_piece::track_piece_vector& track_piece_factory::create_track_piece_vector(const jsoncons::json& data)
{
  track_piece::track_piece_vector* track_pieces = new track_piece::track_piece_vector();

  for (size_t i = 0; i < data.size(); ++i)
  {
    track_pieces->push_back(create_track_piece(data[i]));
  }

  _track_piece_vectors.push_back(track_pieces);
  return _track_piece_vectors.back();
}

