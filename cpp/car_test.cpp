#include "gtest/gtest.h"
#include "car.h"
#include "car_factory.h"

namespace {

class car_test : public ::testing::Test {
protected:
  car_test()
    : factory(),
      car1(factory.create_car(jsoncons::json::parse_string(
        "{ \n"
        "  \"id\": { \n"
        "    \"name\": \"Schumacher\", \n"
        "    \"color\": \"red\" \n"
        "  }, \n"
        "  \"dimensions\": { \n"
        "    \"length\": 40.0, \n"
        "    \"width\": 20.0, \n"
        "    \"guideFlagPosition\": 10.0 \n"
        "  }"
        "}"
      )))
  {
  }

  virtual ~car_test()
  {
  }

  car_factory factory;
  car& car1;
};

TEST_F(car_test, has_correct_values)
{
  EXPECT_EQ("Schumacher", car1.get_name()) << "name is incorrect";
  EXPECT_EQ("red", car1.get_color()) << "color is incorrect";
  EXPECT_EQ(-1, car1.get_piece_index());
  EXPECT_EQ(-1, car1.get_start_lane_index());
  EXPECT_EQ(-1, car1.get_end_lane_index());
  EXPECT_EQ(-1, car1.get_lap());
}

TEST_F(car_test, can_add_lap)
{
  EXPECT_EQ(-1, car1.get_lap_time_ticks(1));
  EXPECT_EQ(-1, car1.get_lap_time_millis(1));

  car1.add_lap(100, 1000);

  EXPECT_EQ(100, car1.get_lap_time_ticks(1));
  EXPECT_EQ(1000, car1.get_lap_time_millis(1));
  EXPECT_EQ(-1, car1.get_lap_time_ticks(2));
  EXPECT_EQ(-1, car1.get_lap_time_millis(2));
}

TEST_F(car_test, handles_set_position)
{
  car1.set_position(jsoncons::json::parse_string(
    " { \n"
    "   \"id\": { \n"
    "     \"name\": \"Schumacher\", \n"
    "     \"color\": \"red\" \n"
    "   }, \n"
    "   \"angle\": 45.0, \n"
    "   \"piecePosition\": { \n"
    "     \"pieceIndex\": 4, \n"
    "     \"inPieceDistance\": 20.0, \n"
    "     \"lane\": { \n"
    "       \"startLaneIndex\": 1, \n"
    "       \"endLaneIndex\": 1 \n"
    "     }, \n"
    "     \"lap\": 1 \n"
    "   } \n"
    " } \n"
  ));

  EXPECT_EQ(45.0, car1.get_angle());
  EXPECT_EQ(4, car1.get_piece_index());
  EXPECT_EQ(20.0, car1.get_in_piece_distance());
  EXPECT_EQ(1, car1.get_start_lane_index());
  EXPECT_EQ(1, car1.get_end_lane_index());
  EXPECT_EQ(1, car1.get_lap());
}

} // namespace

