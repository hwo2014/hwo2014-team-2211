#ifndef HWO_DRIVER_H
#define HWO_DRIVER_H

#include <iostream>
#include <jsoncons/json.hpp>
#include "game.h"

class driver
{
public:
  driver(game& g)
    : _game(g)
  {
  }

  virtual ~driver()
  {
  }

  const game& get_game() const
  {
    return _game;
  }

  virtual jsoncons::json process_tick(long tick) = 0;
  virtual void analyze_track() = 0;

protected:

private:
  game& _game;
};

#endif
