#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>
#include "connection.h"
#include "game.h"
#include "driver.h"

class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  void react(const jsoncons::json& msg, hwo_connection& connection);


private:
  typedef std::function<void(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;

  void on_join(const jsoncons::json& data);
  void on_your_car(const jsoncons::json& data);
  void on_game_init(const jsoncons::json& data);
  void on_game_start(const jsoncons::json& data);
  void on_car_positions(const jsoncons::json& data);
  void on_lap_finished(const jsoncons::json& data);
  void on_crash(const jsoncons::json& data);
  void on_spawn(const jsoncons::json& data);
  void on_finish(const jsoncons::json& data);
  void on_game_end(const jsoncons::json& data);
  void on_tournament_end(const jsoncons::json& data);
  void on_error(const jsoncons::json& data);
  void on_dnf(const jsoncons::json& data);
  void on_turbo_available(const jsoncons::json& data);
  
  track_piece_factory    _tr_pc_factory;
  track_lane_factory     _tr_ln_factory;
  track_factory          _tr_factory;
  car_factory            _cr_factory;
  race_factory           _rc_factory;
  game                   _game;
  std::auto_ptr<driver>  _driver_ptr;
  long                   _tick;
  jsoncons::json         _last_request;
};

#endif
