#ifndef HWO_SPEEDMAP_DRIVER_H
#define HWO_SPEEDMAP_DRIVER_H

#include <boost/ptr_container/ptr_map.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include "base_driver.h"

// Amount of time before a curve that we should brake.
static const double BRAKE_TICKS = 40;

// Ratio of throttle speed management.
static const double DRIFT_SPEED_RATIO = 0.75;

// Maximum amount of drift.
static const double MAX_DRIFT = 30;

// Degrees of angle left of curve before we accelerate.
static const double CURVE_EXIT_ANGLE = 30;

// Maximum amount of drift allowed while exiting a curve.
static const double MAX_CURVE_EXIT_DRIFT = 50;

// Minimum length of straight track that counts as a straightaway.
static const double MIN_STRAIGHTAWAY_LENGTH = 30;

// Maximum distance per tick possible.
static const double MAX_SPEED = 80;

// Smallest radius which can be run at full throttle.
static const double FULL_THROTTLE_RADIUS = 500;

// Multiplier for throttle to speed delta.
// Higher value will cause more throttle modulation.
static const double SPEED_THROTTLE_RATIO = 0.1;

class speed_point
{
public:
  speed_point(int piece_index, double piece_position, double target_speed)
    : _piece_index(piece_index),
      _piece_position(piece_position),
      _target_speed(target_speed)
  {
  }

  int get_piece_index()
  {
    return _piece_index;
  }

  double get_piece_position()
  {
    return _piece_position;
  }

  double get_target_speed()
  {
    return _target_speed;
  }

private:
  int _piece_index;
  double _piece_position;
  double _target_speed;
};

class speedmap_driver : public base_driver
{
public:

  speedmap_driver(game& g);
  virtual ~speedmap_driver();

  virtual void analyze_track();
  virtual jsoncons::json process_tick(long tick);

  void print_status(long tick, double current_speed, double target_speed);
  bool update_throttle(double current_speed, double desired_speed);
  double get_target_speed();
  double get_target_speed(int piece_index, double position);
  double calculate_target_speed(
      const track& trck, const track_piece& piece, int piece_index, double position);
  double calculate_max_speed(
      const track& trck, const track_piece& piece, int piece_index, double position);
  

private:
  typedef boost::ptr_vector<speed_point> speed_point_vector;
  typedef boost::ptr_map<int, speed_point_vector> speed_point_multimap;

  void create_speedmap();

  speed_point_multimap _speed_points;
  double _throttle;
};

#endif
