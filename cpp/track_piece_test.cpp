#include "gtest/gtest.h"
#include "track_piece.h"
#include "track_piece_factory.h"

namespace {

class track_piece_test : public ::testing::Test {
protected:
  track_piece_test()
    : factory(),
      track_pieces(factory.create_track_piece_vector(jsoncons::json::parse_string(
        "[ \n"
        "  { \n"
        "    \"length\": 100.0 \n"
        "  }, \n"
        "  { \n"
        "    \"length\": 100.0, \n"
        "    \"switch\": true \n"
        "  }, \n"
        "  { \n"
        "    \"radius\": 200, \n"
        "    \"angle\": 22.5 \n"
        "  } \n"
        "] \n"
      )))
  {
  }

  virtual ~track_piece_test()
  {
  }

  track_piece_factory factory;
  track_piece::track_piece_vector& track_pieces;
};

TEST_F(track_piece_test, has_three_tracks)
{
  EXPECT_EQ(3, track_pieces.size());
}

TEST_F(track_piece_test, has_correct_values)
{
  EXPECT_EQ(100, track_pieces[0].get_length()) << "piece 0, length incorrect";
  EXPECT_EQ(0, track_pieces[0].get_radius()) << "piece 0, radius incorrect";
  EXPECT_EQ(0, track_pieces[0].get_angle()) << "piece 0, angle incorrect";
  EXPECT_EQ(false, track_pieces[0].is_switch()) << "piece 0, switch incorrect";
}

} // namespace

