#include <cmath>
#include "track_piece.h"

using namespace std;

track_piece::track_piece(const jsoncons::json& data)
{
  _length = data.get("length").as<double>();
  _radius = data.get("radius", 0.0).as<double>();
  _angle = data.get("angle", 0.0).as<double>();
  _switch = data.get("switch", false).as<bool>();

  if (_radius > 0 && _angle != 0)
  {
    double c = M_PI * (_radius * 2);
    double arc = std::abs(_angle) / 360.0;
    _length = c * arc;
  }
}

double track_piece::get_length() const
{
  return _length;
}

double track_piece::get_radius() const
{
  return _radius;
}

double track_piece::get_angle() const
{
  return _angle;
}

bool track_piece::is_switch() const
{
  return _switch;
}

