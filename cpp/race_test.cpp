#include "gtest/gtest.h"
#include "race.h"
#include "race_factory.h"

namespace {

class race_test : public ::testing::Test {
protected:
  race_test()
    : t_p_factory(),
      t_l_factory(),
      t_factory(t_p_factory, t_l_factory),
      c_factory(),
      factory(t_factory, c_factory),
      race(factory.create_race(jsoncons::json::parse_string(
        " { \n"
        "   \"track\": { \n"
        "     \"id\": \"indianapolis\", \n"
        "     \"name\": \"Indianapolis\", \n"
        "     \"pieces\": [ \n"
        "       { \n"
        "         \"length\": 100.0 \n"
        "       }, \n"
        "       { \n"
        "         \"length\": 100.0, \n"
        "         \"switch\": true \n"
        "       }, \n"
        "       { \n"
        "         \"radius\": 200, \n"
        "         \"angle\": 22.5 \n"
        "       } \n"
        "     ], \n"
        "     \"lanes\": [ \n"
        "       { \n"
        "         \"distanceFromCenter\": -20, \n"
        "         \"index\": 0 \n"
        "       }, \n"
        "       { \n"
        "         \"distanceFromCenter\": 0, \n"
        "         \"index\": 1 \n"
        "       }, \n"
        "       { \n"
        "         \"distanceFromCenter\": 20, \n"
        "         \"index\": 2 \n"
        "       } \n"
        "     ], \n"
        "     \"startingPoint\": { \n"
        "       \"position\": { \n"
        "         \"x\": -340.0, \n"
        "         \"y\": -96.0 \n"
        "       }, \n"
        "       \"angle\": 90.0 \n"
        "     } \n"
        "   }, \n"
        "   \"cars\": [ \n"
        "     { \n"
        "       \"id\": { \n"
        "         \"name\": \"Schumacher\", \n"
        "         \"color\": \"red\" \n"
        "       }, \n"
        "       \"dimensions\": { \n"
        "         \"length\": 40.0, \n"
        "         \"width\": 20.0, \n"
        "         \"guideFlagPosition\": 10.0 \n"
        "       } \n"
        "     }, \n"
        "     { \n"
        "       \"id\": { \n"
        "         \"name\": \"Rosberg\", \n"
        "         \"color\": \"blue\" \n"
        "       }, \n"
        "       \"dimensions\": { \n"
        "         \"length\": 40.0, \n"
        "         \"width\": 20.0, \n"
        "         \"guideFlagPosition\": 10.0 \n"
        "       } \n"
        "     } \n"
        "   ], \n"
        "   \"raceSession\": { \n"
        "     \"laps\": 3, \n"
        "     \"maxLapTimeMs\": 30000, \n"
        "     \"quickRace\": true \n"
        "   } \n"
        " } \n"
      )))
  {
  }

  virtual ~race_test()
  {
  }

  track_piece_factory t_p_factory;
  track_lane_factory t_l_factory;
  track_factory t_factory;
  car_factory c_factory;
  race_factory factory;
  race& race;
};

TEST_F(race_test, has_correct_values)
{
  EXPECT_EQ(3, race.get_laps());
  EXPECT_EQ(30000, race.get_max_lap_time_ms());
  EXPECT_EQ(true, race.is_quick_race());
}

TEST_F(race_test, can_set_millis)
{
  EXPECT_EQ(0, race.get_millis());

  race.set_millis(456);
  EXPECT_EQ(456, race.get_millis());
}

TEST_F(race_test, has_track)
{
  const track& t = race.get_track();
  EXPECT_EQ("indianapolis", t.get_id());
  EXPECT_EQ(3, t.get_lanes().size());
  EXPECT_EQ(3, t.get_pieces().size());
}

TEST_F(race_test, has_cars)
{
  EXPECT_EQ(2, race.get_cars().size());
  EXPECT_EQ("Schumacher", race.get_cars()[0].get_name());
  EXPECT_EQ("Rosberg", race.get_cars()[1].get_name());
}

TEST_F(race_test, can_get_cars_by_color)
{
  EXPECT_EQ("Schumacher", race.get_car_by_color("red").get_name());
  EXPECT_EQ("Rosberg", race.get_car_by_color("blue").get_name());
}

} // namespace

