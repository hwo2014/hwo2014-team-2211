#ifndef HWO_BASE_DRIVER_H
#define HWO_BASE_DRIVER_H

#include <cmath>
#include <iostream>
#include "driver.h"
#include "protocol.h"
#include "base_driver.h"

base_driver::base_driver(game& g)
  : driver(g),
    _last_speed_tick_index(0),
    _last_piece_index(-1),
    _last_piece_distance(-1)
{
  // Initialize speed_ticks array.
  for (int i = 0; i < SPEED_AVG_TICK_COUNT; i++) {
    _speed_ticks[i] = 0;
  }
}

base_driver::~base_driver()
{
}

void base_driver::analyze_track()
{
}

jsoncons::json base_driver::process_tick(long tick)
{
  log_speed();

  return hwo_protocol::make_ping();
}

double base_driver::get_distance_per_tick()
{
  double sum = 0.0;

  for (int i = 0; i < SPEED_AVG_TICK_COUNT; i++)
  {
    sum += _speed_ticks[i];
  }

  return (sum / (double)SPEED_AVG_TICK_COUNT);
}

void base_driver::log_speed()
{
  // increment to next element in circular array.
  int index = (_last_speed_tick_index == (SPEED_AVG_TICK_COUNT - 1) ? 0 : _last_speed_tick_index + 1);
  double distance = calculate_distance_covered();
  _speed_ticks[index] = distance;
  _last_speed_tick_index = index;
}

/**
 * Calculates the distance covered since last tick.
 */
double base_driver::calculate_distance_covered()
{
  double distance = 0.0;
  const track& track = get_game().get_race().get_track();
  const car& my_car = get_game().get_my_car();
  int piece_index = my_car.get_piece_index();
  double piece_distance = my_car.get_in_piece_distance();

  if (_last_piece_index > -1)
  {
    distance = calculate_distance(
                  track,
                  _last_piece_index,
                  _last_piece_distance,
                  piece_index,
                  piece_distance);
  }

  _last_piece_index = piece_index;
  _last_piece_distance = piece_distance;

  return distance;
}

double base_driver::calculate_distance(
    const track& track,
    int start_piece_index,
    double start_piece_distance,
    int end_piece_index,
    double end_piece_distance) const
{
  double distance = 0.0;

  int i = start_piece_index;
  while(true)
  {
    const track_piece& piece = track.get_pieces()[i];

    if (i == end_piece_index)
    {
      distance += end_piece_distance;
    }
    else if (i == start_piece_index)
    {
      distance += piece.get_length() - start_piece_distance;
    }
    else
    {
      distance += piece.get_length();
    }

    // If we've just added the end piece, we're done.
    if (i == end_piece_index)
    {
      break;
    }
    else
    {
      // Increment i, but loop back to 0 if we hit the end.
      i = (i == (track.get_pieces().size() - 1) ? 0 : i + 1);
    }
  }

  return distance;
}

int base_driver::get_piece_index(double look_ahead_distance)
{
  const track& track = get_game().get_race().get_track();
  const car& my_car = get_game().get_my_car();
  int piece_index = my_car.get_piece_index();
  double piece_distance = my_car.get_in_piece_distance();
  return get_piece_index(track, piece_index, piece_distance, look_ahead_distance);
}

int base_driver::get_piece_index(
  const track& track,
  int start_piece_index,
  double start_piece_distance,
  double look_ahead_distance)
{
  double distance_left = look_ahead_distance;
  int index = start_piece_index;
  do
  {
    const track_piece& piece = track.get_pieces()[index];
    if (index == start_piece_index)
    {
      distance_left -= (piece.get_length() - start_piece_distance);
    }
    else
    {
      distance_left -= piece.get_length();
    }

    if (distance_left > 0)
    {
      index = (index == (track.get_pieces().size() - 1) ? 0 : index + 1);
    }
  } while (distance_left > 0);

  return index;
}

#endif
