#include "track_lane_factory.h"
#include "track_lane.h"

using namespace std;

track_lane_factory::track_lane_factory()
{
}

track_lane& track_lane_factory::create_track_lane(const jsoncons::json& data)
{
  _track_lanes.push_back(new track_lane(data));
  return _track_lanes.back();
}

track_lane::track_lane_vector& track_lane_factory::create_track_lane_vector(const jsoncons::json& data)
{
  track_lane::track_lane_vector* track_lanes = new track_lane::track_lane_vector();

  for (size_t i = 0; i < data.size(); ++i)
  {
    track_lanes->push_back(create_track_lane(data[i]));
  }

  _track_lane_vectors.push_back(track_lanes);
  return _track_lane_vectors.back();
}

