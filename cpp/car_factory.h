#ifndef HWO_CAR_FACTORY_H
#define HWO_CAR_FACTORY_H

#include <boost/ptr_container/ptr_vector.hpp>
#include <iostream>
#include <jsoncons/json.hpp>
#include "car.h"

class car_factory
{
public:
  car_factory();

  car& create_car(const jsoncons::json& data);
  car::car_vector& create_car_vector(const jsoncons::json& data);

private:
  boost::ptr_vector<car> _cars;
  boost::ptr_vector<car::car_vector> _car_vectors;
};

#endif
