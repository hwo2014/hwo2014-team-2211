#ifndef HWO_TRACK_H
#define HWO_TRACK_H

#include <string>
#include <iostream>
#include <jsoncons/json.hpp>
#include "track_piece.h"
#include "track_piece_factory.h"
#include "track_lane.h"
#include "track_lane_factory.h"

class track
{
public:
  track(const jsoncons::json& data, track_piece_factory& track_piece_f, track_lane_factory& track_lane_f);

  std::string get_id() const;
  std::string get_name() const;
  const track_piece::track_piece_vector& get_pieces() const;
  const track_lane::track_lane_vector& get_lanes() const;

private:
  std::string _id;
  std::string _name;
  track_piece::track_piece_vector& _pieces;
  track_lane::track_lane_vector& _lanes;
    
};

#endif
