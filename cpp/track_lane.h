#ifndef HWO_TRACK_LANE_H
#define HWO_TRACK_LANE_H

#include <iostream>
#include <vector>
#include <jsoncons/json.hpp>

/**
 * Represents a slot lane for the track.
 */
class track_lane
{
public:
  typedef std::vector<track_lane> track_lane_vector;

  track_lane(const jsoncons::json& data);

  int get_index() const;
  double get_distance_from_center() const;

private:
  int _index;
  double _distance_from_center;
};

#endif
