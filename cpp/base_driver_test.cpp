#include <cmath>
#include "gtest/gtest.h"
#include "game.h"
#include "base_driver.h"

namespace {

class base_driver_test : public ::testing::Test {
protected:
  base_driver_test()
    : _t_p_factory(),
      _t_l_factory(),
      _t_factory(_t_p_factory, _t_l_factory),
      _c_factory(),
      _r_factory(_t_factory, _c_factory),
      _game(_r_factory),
      _driver(_game)
  {
  }

  virtual ~base_driver_test()
  {
  }

  void game_init();

  track_piece_factory _t_p_factory;
  track_lane_factory _t_l_factory;
  track_factory _t_factory;
  car_factory _c_factory;
  race_factory _r_factory;
  game _game;
  base_driver _driver;
};

void base_driver_test::game_init()
{
  _game.set_game_init_data(jsoncons::json::parse_string(
    " { \n"
    " \"race\": { \n"
    "   \"track\": { \n"
    "     \"id\": \"indianapolis\", \n"
    "     \"name\": \"Indianapolis\", \n"
    "     \"pieces\": [ \n"
    "       { \n"
    "         \"length\": 100.0 \n"
    "       }, \n"
    "       { \n"
    "         \"length\": 100.0, \n"
    "         \"switch\": true \n"
    "       }, \n"
    "       { \n"
    "         \"radius\": 150, \n"
    "         \"angle\": 90 \n"
    "       }, \n"
    "       { \n"
    "         \"length\": 50.0 \n"
    "       }, \n"
    "       { \n"
    "         \"radius\": 100, \n"
    "         \"angle\": 45 \n"
    "       }, \n"
    "       { \n"
    "         \"length\": 75.0 \n"
    "       } \n"
    "     ], \n"
    "     \"lanes\": [ \n"
    "       { \n"
    "         \"distanceFromCenter\": -20, \n"
    "         \"index\": 0 \n"
    "       }, \n"
    "       { \n"
    "         \"distanceFromCenter\": 0, \n"
    "         \"index\": 1 \n"
    "       }, \n"
    "       { \n"
    "         \"distanceFromCenter\": 20, \n"
    "         \"index\": 2 \n"
    "       } \n"
    "     ], \n"
    "     \"startingPoint\": { \n"
    "       \"position\": { \n"
    "         \"x\": -340.0, \n"
    "         \"y\": -96.0 \n"
    "       }, \n"
    "       \"angle\": 90.0 \n"
    "     } \n"
    "   }, \n"
    "   \"cars\": [ \n"
    "     { \n"
    "       \"id\": { \n"
    "         \"name\": \"Schumacher\", \n"
    "         \"color\": \"red\" \n"
    "       }, \n"
    "       \"dimensions\": { \n"
    "         \"length\": 40.0, \n"
    "         \"width\": 20.0, \n"
    "         \"guideFlagPosition\": 10.0 \n"
    "       } \n"
    "     }, \n"
    "     { \n"
    "       \"id\": { \n"
    "         \"name\": \"Rosberg\", \n"
    "         \"color\": \"blue\" \n"
    "       }, \n"
    "       \"dimensions\": { \n"
    "         \"length\": 40.0, \n"
    "         \"width\": 20.0, \n"
    "         \"guideFlagPosition\": 10.0 \n"
    "       } \n"
    "     } \n"
    "   ], \n"
    "   \"raceSession\": { \n"
    "     \"laps\": 3, \n"
    "     \"maxLapTimeMs\": 30000, \n"
    "     \"quickRace\": true \n"
    "   } \n"
    " }} \n"
  ));
}

TEST_F(base_driver_test, can_calculate_distance)
{
  game_init();

  EXPECT_EQ(50, _driver.calculate_distance(_game.get_race().get_track(), 0, 0, 0, 50));
  EXPECT_EQ(75, _driver.calculate_distance(_game.get_race().get_track(), 0, 25, 1, 0));
  EXPECT_EQ(125, _driver.calculate_distance(_game.get_race().get_track(), 0, 25, 1, 50));
  EXPECT_EQ(40, _driver.calculate_distance(_game.get_race().get_track(), 2, 0, 2, 40));
  EXPECT_DOUBLE_EQ((M_PI * (150 * 2)) / 4,
                   _driver.calculate_distance(_game.get_race().get_track(), 2, 0, 3, 0));
}

TEST_F(base_driver_test, can_get_piece_index)
{
  game_init();

  const track& track = _game.get_race().get_track();

  // Test that it advances within the same piece.
  EXPECT_EQ(0, _driver.get_piece_index(track, 0, 50, 25));

  // Test that it advances 1 past the current piece.
  EXPECT_EQ(1, _driver.get_piece_index(track, 0, 50, 75));

  // Test that it advances 2 past the current piece.
  EXPECT_EQ(2, _driver.get_piece_index(track, 0, 50, 175));

  // Test over a single curve piece.
  EXPECT_EQ(2, _driver.get_piece_index(track, 2, 15, 10));

  // Test that the track wraps back to index 0.
  EXPECT_EQ(0, _driver.get_piece_index(track, 5, 50, 50));
}

TEST_F(base_driver_test, can_get_piece_ahead)
{
  game_init();

  const track& track = _game.get_race().get_track();

  EXPECT_EQ(0, _driver.get_piece_index(track, 0, 0, 0));
  EXPECT_EQ(1, _driver.get_piece_index(track, 0, 50, 75));
  EXPECT_EQ(2, _driver.get_piece_index(track, 1, 50, 75));
  EXPECT_EQ(2, _driver.get_piece_index(track, 2, 0, 0));
}

} // namespace

