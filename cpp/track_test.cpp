#include "gtest/gtest.h"
#include "track.h"
#include "track_factory.h"

namespace {

class track_test : public ::testing::Test {
protected:
  track_test()
    : piece_factory(),
      lane_factory(),
      factory(piece_factory, lane_factory),
      track(factory.create_track(jsoncons::json::parse_string(
        " { \n"
        "   \"id\": \"indianapolis\", \n"
        "   \"name\": \"Indianapolis\", \n"
        "   \"pieces\": [ \n"
        "     { \n"
        "       \"length\": 100.0 \n"
        "     }, \n"
        "     { \n"
        "       \"length\": 100.0, \n"
        "       \"switch\": true \n"
        "     }, \n"
        "     { \n"
        "       \"radius\": 200, \n"
        "       \"angle\": 22.5 \n"
        "     } \n"
        "   ], \n"
        "   \"lanes\": [ \n"
        "     { \n"
        "       \"distanceFromCenter\": -20, \n"
        "       \"index\": 0 \n"
        "     }, \n"
        "     { \n"
        "       \"distanceFromCenter\": 0, \n"
        "       \"index\": 1 \n"
        "     }, \n"
        "     { \n"
        "       \"distanceFromCenter\": 20, \n"
        "       \"index\": 2 \n"
        "     } \n"
        "   ], \n"
        "   \"startingPoint\": { \n"
        "     \"position\": { \n"
        "       \"x\": -340.0, \n"
        "       \"y\": -96.0 \n"
        "     }, \n"
        "     \"angle\": 90.0 \n"
        "   } \n"
        " } \n"
      )))
  {
  }

  virtual ~track_test()
  {
  }

  track_piece_factory piece_factory;
  track_lane_factory lane_factory;
  track_factory factory;
  track& track;
};

TEST_F(track_test, has_correct_values)
{
  EXPECT_EQ("indianapolis", track.get_id());
  EXPECT_EQ("Indianapolis", track.get_name());
}

TEST_F(track_test, has_pieces)
{
  EXPECT_EQ(3, track.get_pieces().size());
}

TEST_F(track_test, has_lanes)
{
  EXPECT_EQ(3, track.get_lanes().size());
}

} // namespace

