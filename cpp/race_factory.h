#ifndef HWO_RACE_FACTORY_H
#define HWO_RACE_FACTORY_H

#include <boost/ptr_container/ptr_vector.hpp>
#include <iostream>
#include <jsoncons/json.hpp>
#include "race.h"
#include "track_factory.h"
#include "car_factory.h"

class race_factory
{
public:
  race_factory(track_factory& track_f, car_factory& car_f);

  race& create_race(const jsoncons::json& data);

private:
  boost::ptr_vector<race> _races;
  track_factory& _track_f;
  car_factory& _car_f;
};

#endif
