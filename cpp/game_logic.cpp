#include "game_logic.h"
#include "protocol.h"

extern driver* create_driver(game& g);

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "joinRace", &game_logic::on_join },
      { "yourCar", &game_logic::on_your_car },
      { "gameInit", &game_logic::on_game_init },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "lapFinished", &game_logic::on_lap_finished },
      { "finish", &game_logic::on_finish },
      { "crash", &game_logic::on_crash },
      { "spawn" , &game_logic::on_spawn },
      { "gameEnd", &game_logic::on_game_end },
      { "tournamentEnd", &game_logic::on_tournament_end },
      { "error", &game_logic::on_error },
      { "dnf", &game_logic::on_dnf },
      {"turboAvailable" , &game_logic::on_turbo_available }
    },
     _tr_pc_factory(),
     _tr_ln_factory(),
     _tr_factory(_tr_pc_factory, _tr_ln_factory),
     _cr_factory(),
     _rc_factory(_tr_factory, _cr_factory),
     _game(_rc_factory),
     _driver_ptr(create_driver(_game)),
     _tick(-1),
     _last_request()
{
}

void game_logic::react(const jsoncons::json& msg, hwo_connection& connection)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  const auto& game_tick = msg.get("gameTick", _tick).as<long>();
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    try
    {
      (action_it->second)(this, data);
    }
    catch (const jsoncons::json_parse_exception& jpe)
    {
      std::cerr << "Error on message: " << std::endl
                << data.to_string() << std::endl;
      std::cerr << "Exception: " << jpe.message() << std::endl;
    }
  }
  else
  {
    std::cout << std::endl << "Unknown message type: " << msg_type << std::endl;
  }

  if (_tick != game_tick)
  {
    _tick = game_tick;
    _last_request = _driver_ptr->process_tick(_tick);
    connection.send_requests({ _last_request });
  }
}

void game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
}

void game_logic::on_your_car(const jsoncons::json& data)
{
  _game.set_your_car_data(data);
  std::cout << "Your Car" << std::endl;
}

void game_logic::on_game_init(const jsoncons::json& data)
{
  _game.set_game_init_data(data);
  //std::cout << "Result: " << data.to_string() << std::endl; // debug ss
  _driver_ptr->analyze_track();
  std::cout << "Game Init" << std::endl;
}

void game_logic::on_game_start(const jsoncons::json& data)
{
  _game.set_game_start_data(data);
  std::cout << "Race started" << std::endl;
}

void game_logic::on_car_positions(const jsoncons::json& data)
{
  _game.set_car_positions_data(data);
}

void game_logic::on_lap_finished(const jsoncons::json& data)
{
  _game.set_lap_finished_data(data);
  std::cout << "Lap finished" << std::endl;
}

void game_logic::on_crash(const jsoncons::json& data)
{
  _game.set_crash_data(data);
  std::cout << "Someone crashed" << std::endl;
}

void game_logic::on_spawn(const jsoncons::json& data)
{
    _game.set_spawn_data(data);
    std::cout << "Someone spawned" << std::endl;
}

void game_logic::on_finish(const jsoncons::json& data)
{
  _game.set_finish_data(data);
  std::cout << "On Finish" << std::endl;
}

void game_logic::on_game_end(const jsoncons::json& data)
{
  _game.set_game_end_data(data);
  std::cout << "Race ended" << std::endl;
  std::cout << "Result: " << data.to_string() << std::endl;
}

void game_logic::on_tournament_end(const jsoncons::json& data)
{
  _game.set_tournament_end_data(data);
  std::cout << "Tournament ended" << std::endl;
}

void game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  std::cout << "Last Request: " << _last_request.to_string() << std::endl;
}

void game_logic::on_dnf(const jsoncons::json& data)
{
  _game.set_dnf_data(data);
  std::cout << "Someone disqualified" << std::endl;
}

void game_logic::on_turbo_available(const jsoncons::json& data)
{
  _game.set_turbo_available_data(data);
  std::cout << "Turbo: " << data.to_string() << std::endl;
}
