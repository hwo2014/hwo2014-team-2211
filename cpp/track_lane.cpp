#include "track_lane.h"

track_lane::track_lane(const jsoncons::json& data)
{
  _index = data["index"].as<int>();
  _distance_from_center = data["distanceFromCenter"].as<double>();
}

int track_lane::get_index() const
{
  return _index;
}

double track_lane::get_distance_from_center() const
{
  return _distance_from_center;
}

