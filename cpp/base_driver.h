#include "driver.h"

// Number of ticks to average out to calculate speed.
static const int SPEED_AVG_TICK_COUNT = 10;

// The maximum throttle we can set.
static const double MAX_THROTTLE = 1.0;

// The minimum throttle we can set.
static const double MIN_THROTTLE = 0.0;

class base_driver : public driver
{
public:

  base_driver(game& g);
  virtual ~base_driver();

  virtual void analyze_track();
  virtual jsoncons::json process_tick(long tick);

  // Get the current speed of the car.
  double get_distance_per_tick();

  // Calculates distance between one position on track and another.
  double calculate_distance(
    const track& track,
    int start_piece_index,
    double start_piece_distance,
    int end_piece_index,
    double end_piece_distance) const;

  // Gets a piece index by the distance ahead of the current position.
  int get_piece_index(double look_ahead_distance);

  // Gets a piece index relative to a position on the track.
  int get_piece_index(
    const track& track,
    int start_piece_index,
    double start_piece_distance,
    double look_ahead_distance);

private:
  void log_speed();
  double calculate_distance_covered();

  double _speed_ticks[SPEED_AVG_TICK_COUNT];
  int _last_speed_tick_index;
  int _last_piece_index;
  int _last_piece_distance;
};
