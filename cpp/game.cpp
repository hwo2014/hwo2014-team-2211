#include "game.h"

using namespace std;

game::game(race_factory& race_f)
  : _race_f(race_f),
    _my_car_name(""),
    _my_car_color(""),
    _race(NULL),
    _start_time(-1),
    _end_time(-1),
    _race_results(),
    _best_laps(),
    _turbo_duration_ticks(0),
    _turbo_factor(0.0)
{
}  

std::string game::get_my_car_name() const
{
  return _my_car_name;
}

std::string game::get_my_car_color() const
{
  return _my_car_color;
}

bool game::is_initialized() const
{
  return _race != NULL;
}

car& game::get_my_car() const
{
  return get_race().get_car_by_color(get_my_car_color());
}


race& game::get_race() const
{
  return *_race;
}

time_t game::get_start_time() const
{
  return _start_time;
}

time_t game::get_end_time() const
{
  return _end_time;
}

const jsoncons::json& game::get_race_results() const
{
  return _race_results;
}

const jsoncons::json& game::get_best_laps() const
{
  return _best_laps;
}

long game::get_turbo_duration_ticks() const
{
  return _turbo_duration_ticks;
}

double game::get_turbo_factor() const
{
  return _turbo_factor;
}

void game::clear_turbo()
{
  _turbo_duration_ticks = 0;
  _turbo_factor = 0.0;
}

void game::set_your_car_data(const jsoncons::json& data)
{
  _my_car_name = data["name"].as<string>();
  _my_car_color = data["color"].as<string>();
}

void game::set_game_init_data(const jsoncons::json& data)
{
  _race = &_race_f.create_race(data["race"]);
}

void game::set_game_start_data(const jsoncons::json& data)
{
  _start_time = time(0);
}

void game::set_car_positions_data(const jsoncons::json& data)
{
  for (size_t i = 0; i < data.size(); ++i)
  {
    car& c = get_race().get_car_by_color(data[i]["id"]["color"].as<string>());
    c.set_position(data[i]);
  }
}

void game::set_turbo_available_data(const jsoncons::json& data)
{
  _turbo_duration_ticks = data["turboDurationTicks"].as<long>();
  _turbo_factor = data["turboFactor"].as<double>();
}

void game::set_game_end_data(const jsoncons::json& data)
{
  _end_time = time(0);
  _race_results = data["results"];
  _best_laps = data["bestLaps"];
}

void game::set_tournament_end_data(const jsoncons::json& data)
{
  // Nothing to do here.
}

void game::set_crash_data(const jsoncons::json& data)
{
  car& c = get_race().get_car_by_color(data["color"].as<string>());
  c.set_crashed(true);
}

void game::set_spawn_data(const jsoncons::json& data)
{
  car& c = get_race().get_car_by_color(data["color"].as<string>());
  c.set_crashed(false);
}

void game::set_lap_finished_data(const jsoncons::json& data)
{
  const jsoncons::json& lap_time = data["lapTime"];
  car& c = get_race().get_car_by_color(data["car"]["color"].as<string>());
  c.add_lap(lap_time["ticks"].as<long>(),
            lap_time["millis"].as<long>());
  c.update_ranking(data["ranking"]["overall"].as<int>(),
                   data["ranking"]["fastestLap"].as<int>());
  get_race().set_millis(data["raceTime"]["millis"].as<long>());
}

void game::set_dnf_data(const jsoncons::json& data)
{
  car& c = get_race().get_car_by_color(data["car"]["color"].as<string>());
  c.set_disqualified(true, data["reason"].as<string>());
}

void game::set_finish_data(const jsoncons::json& data)
{
  car& c = get_race().get_car_by_color(data["color"].as<string>());
  c.set_finished(true);
}

