#include "gtest/gtest.h"
#include "game.h"

using namespace std;

namespace {

class game_test : public ::testing::Test {
protected:
  game_test()
    : _t_p_factory(),
      _t_l_factory(),
      _t_factory(_t_p_factory, _t_l_factory),
      _c_factory(),
      _r_factory(_t_factory, _c_factory),
      _game(_r_factory)
  {
  }

  virtual ~game_test()
  {
  }

  void game_init();

  track_piece_factory _t_p_factory;
  track_lane_factory _t_l_factory;
  track_factory _t_factory;
  car_factory _c_factory;
  race_factory _r_factory;
  game _game;
};

TEST_F(game_test, has_initial_values)
{
  EXPECT_EQ(NULL, &_game.get_race());
  EXPECT_EQ(-1, _game.get_start_time());
}

void game_test::game_init()
{
  _game.set_game_init_data(jsoncons::json::parse_string(
    " { \n"
    " \"race\": { \n"
    "   \"track\": { \n"
    "     \"id\": \"indianapolis\", \n"
    "     \"name\": \"Indianapolis\", \n"
    "     \"pieces\": [ \n"
    "       { \n"
    "         \"length\": 100.0 \n"
    "       }, \n"
    "       { \n"
    "         \"length\": 100.0, \n"
    "         \"switch\": true \n"
    "       }, \n"
    "       { \n"
    "         \"radius\": 200, \n"
    "         \"angle\": 22.5 \n"
    "       } \n"
    "     ], \n"
    "     \"lanes\": [ \n"
    "       { \n"
    "         \"distanceFromCenter\": -20, \n"
    "         \"index\": 0 \n"
    "       }, \n"
    "       { \n"
    "         \"distanceFromCenter\": 0, \n"
    "         \"index\": 1 \n"
    "       }, \n"
    "       { \n"
    "         \"distanceFromCenter\": 20, \n"
    "         \"index\": 2 \n"
    "       } \n"
    "     ], \n"
    "     \"startingPoint\": { \n"
    "       \"position\": { \n"
    "         \"x\": -340.0, \n"
    "         \"y\": -96.0 \n"
    "       }, \n"
    "       \"angle\": 90.0 \n"
    "     } \n"
    "   }, \n"
    "   \"cars\": [ \n"
    "     { \n"
    "       \"id\": { \n"
    "         \"name\": \"Schumacher\", \n"
    "         \"color\": \"red\" \n"
    "       }, \n"
    "       \"dimensions\": { \n"
    "         \"length\": 40.0, \n"
    "         \"width\": 20.0, \n"
    "         \"guideFlagPosition\": 10.0 \n"
    "       } \n"
    "     }, \n"
    "     { \n"
    "       \"id\": { \n"
    "         \"name\": \"Rosberg\", \n"
    "         \"color\": \"blue\" \n"
    "       }, \n"
    "       \"dimensions\": { \n"
    "         \"length\": 40.0, \n"
    "         \"width\": 20.0, \n"
    "         \"guideFlagPosition\": 10.0 \n"
    "       } \n"
    "     } \n"
    "   ], \n"
    "   \"raceSession\": { \n"
    "     \"laps\": 3, \n"
    "     \"maxLapTimeMs\": 30000, \n"
    "     \"quickRace\": true \n"
    "   } \n"
    " }} \n"
  ));
}

TEST_F(game_test, sets_your_car)
{
  _game.set_your_car_data(jsoncons::json::parse_string(
    " { \n"
    "   \"name\": \"Schumacher\", \n"
    "   \"color\": \"red\" \n"
    " } \n"
  ));

  EXPECT_EQ("Schumacher", _game.get_my_car_name());
  EXPECT_EQ("red", _game.get_my_car_color());
  EXPECT_EQ(false, _game.is_initialized());

  game_init();
  
  EXPECT_EQ(true, _game.is_initialized());
  EXPECT_EQ("Schumacher", _game.get_my_car().get_name());
}

TEST_F(game_test, sets_game_init)
{
  game_init();

  EXPECT_EQ(3, _game.get_race().get_laps());
  EXPECT_EQ(true, _game.get_race().is_quick_race());
  EXPECT_EQ(2, _game.get_race().get_cars().size());
}

TEST_F(game_test, sets_game_start)
{
  _game.set_game_start_data(jsoncons::json::parse_string(
    " null \n"
  ));

  // Time should not be zero, but should be same as or earlier than now.
  EXPECT_NE(0, _game.get_start_time());
  EXPECT_LE(_game.get_start_time(), time(0));
}

TEST_F(game_test, sets_car_positions)
{
  game_init();

  _game.set_car_positions_data(jsoncons::json::parse_string(
    " [ \n"
    "   { \n"
    "     \"id\": { \n"
    "       \"name\": \"Schumacher\", \n"
    "       \"color\": \"red\" \n"
    "     }, \n"
    "     \"angle\": 0.0, \n"
    "     \"piecePosition\": { \n"
    "       \"pieceIndex\": 0, \n"
    "       \"inPieceDistance\": 0.0, \n"
    "       \"lane\": { \n"
    "         \"startLaneIndex\": 0, \n"
    "         \"endLaneIndex\": 0 \n"
    "       }, \n"
    "       \"lap\": 1 \n"
    "     } \n"
    "   }, \n"
    "   { \n"
    "     \"id\": { \n"
    "       \"name\": \"Rosberg\", \n"
    "       \"color\": \"blue\" \n"
    "     }, \n"
    "     \"angle\": 45.0, \n"
    "     \"piecePosition\": { \n"
    "       \"pieceIndex\": 0, \n"
    "       \"inPieceDistance\": 20.0, \n"
    "       \"lane\": { \n"
    "         \"startLaneIndex\": 1, \n"
    "         \"endLaneIndex\": 1 \n"
    "       }, \n"
    "       \"lap\": 0 \n"
    "     } \n"
    "   } \n"
    " ] \n"
  ));

  car& red_car = _game.get_race().get_car_by_color("red");
  car& blue_car = _game.get_race().get_car_by_color("blue");

  EXPECT_EQ(0.0, red_car.get_angle());
  EXPECT_EQ(0, red_car.get_piece_index());
  EXPECT_EQ(0.0, red_car.get_in_piece_distance());
  EXPECT_EQ(0, red_car.get_start_lane_index());
  EXPECT_EQ(0, red_car.get_end_lane_index());
  EXPECT_EQ(1, red_car.get_lap());

  EXPECT_EQ(45.0, blue_car.get_angle());
  EXPECT_EQ(0, blue_car.get_piece_index());
  EXPECT_EQ(20.0, blue_car.get_in_piece_distance());
  EXPECT_EQ(1, blue_car.get_start_lane_index());
  EXPECT_EQ(1, blue_car.get_end_lane_index());
  EXPECT_EQ(0, blue_car.get_lap());
}

TEST_F(game_test, sets_game_end)
{
  _game.set_game_end_data(jsoncons::json::parse_string(
    " { \n"
    "   \"results\": [ \n"
    "     { \n"
    "       \"car\": { \n"
    "         \"name\": \"Schumacher\", \n"
    "         \"color\": \"red\" \n"
    "       }, \n"
    "       \"result\": { \n"
    "         \"laps\": 3, \n"
    "         \"ticks\": 9999, \n"
    "         \"millis\": 45245 \n"
    "       } \n"
    "     }, \n"
    "     { \n"
    "       \"car\": { \n"
    "         \"name\": \"Rosberg\", \n"
    "         \"color\": \"blue\" \n"
    "       }, \n"
    "       \"result\": {} \n"
    "     } \n"
    "   ], \n"
    "   \"bestLaps\": [ \n"
    "     { \n"
    "       \"car\": { \n"
    "         \"name\": \"Schumacher\", \n"
    "         \"color\": \"red\" \n"
    "       }, \n"
    "       \"result\": { \n"
    "         \"lap\": 2, \n"
    "         \"ticks\": 3333, \n"
    "         \"millis\": 20000 \n"
    "       } \n"
    "     }, \n"
    "     { \n"
    "       \"car\": { \n"
    "         \"name\": \"Rosberg\", \n"
    "         \"color\": \"blue\" \n"
    "       }, \n"
    "       \"result\": {} \n"
    "     } \n"
    "   ] \n"
    " } \n"
  ));

  EXPECT_EQ(2, _game.get_race_results().size());
  EXPECT_EQ(2, _game.get_best_laps().size());

  EXPECT_EQ("red", _game.get_race_results()[0]["car"]["color"].as<string>());
  EXPECT_EQ(3, _game.get_race_results()[0]["result"]["laps"].as<int>());
  EXPECT_EQ(9999, _game.get_race_results()[0]["result"]["ticks"].as<int>());
  EXPECT_EQ(45245, _game.get_race_results()[0]["result"]["millis"].as<int>());

  EXPECT_EQ("blue", _game.get_race_results()[1]["car"]["color"].as<string>());
  EXPECT_EQ(true, _game.get_race_results()[1]["result"].is_empty());

  EXPECT_EQ("red", _game.get_best_laps()[0]["car"]["color"].as<string>());
  EXPECT_EQ(2, _game.get_best_laps()[0]["result"]["lap"].as<int>());
  EXPECT_EQ(3333, _game.get_best_laps()[0]["result"]["ticks"].as<int>());
  EXPECT_EQ(20000, _game.get_best_laps()[0]["result"]["millis"].as<int>());

  EXPECT_EQ("blue", _game.get_best_laps()[1]["car"]["color"].as<string>());
  EXPECT_EQ(true, _game.get_best_laps()[1]["result"].is_empty());
}

TEST_F(game_test, sets_crash_and_spawn)
{
  game_init();

  EXPECT_EQ(false, _game.get_race().get_car_by_color("blue").is_crashed());
  EXPECT_EQ(0, _game.get_race().get_car_by_color("blue").get_crash_count());

  _game.set_crash_data(jsoncons::json::parse_string(
    " { \n"
    "   \"name\": \"Rosberg\", \n"
    "   \"color\": \"blue\" \n"
    " } \n"
  ));

  EXPECT_EQ(true, _game.get_race().get_car_by_color("blue").is_crashed());
  EXPECT_EQ(1, _game.get_race().get_car_by_color("blue").get_crash_count());

  _game.set_spawn_data(jsoncons::json::parse_string(
    " { \n"
    "   \"name\": \"Rosberg\", \n"
    "   \"color\": \"blue\" \n"
    " } \n"
  ));

  EXPECT_EQ(false, _game.get_race().get_car_by_color("blue").is_crashed());

  _game.set_crash_data(jsoncons::json::parse_string(
    " { \n"
    "   \"name\": \"Rosberg\", \n"
    "   \"color\": \"blue\" \n"
    " } \n"
  ));

  EXPECT_EQ(true, _game.get_race().get_car_by_color("blue").is_crashed());
  EXPECT_EQ(2, _game.get_race().get_car_by_color("blue").get_crash_count());
}

TEST_F(game_test, sets_lap_finished)
{
  game_init();

  _game.set_lap_finished_data(jsoncons::json::parse_string(
    " { \n"
    "   \"car\": { \n"
    "     \"name\": \"Schumacher\", \n"
    "     \"color\": \"red\" \n"
    "   }, \n"
    "   \"lapTime\": { \n"
    "     \"lap\": 1, \n"
    "     \"ticks\": 666, \n"
    "     \"millis\": 6660 \n"
    "   }, \n"
    "   \"raceTime\": { \n"
    "     \"laps\": 1, \n"
    "     \"ticks\": 1250, \n"
    "     \"millis\": 12500 \n"
    "   }, \n"
    "   \"ranking\": { \n"
    "     \"overall\": 1, \n"
    "     \"fastestLap\": 1 \n"
    "   } \n"
    " } \n"
  ));

  EXPECT_EQ(666, _game.get_race().get_car_by_color("red").get_lap_time_ticks(1));
  EXPECT_EQ(6660, _game.get_race().get_car_by_color("red").get_lap_time_millis(1));
  EXPECT_EQ(12500, _game.get_race().get_millis());
}

TEST_F(game_test, sets_dnf)
{
  game_init();

  _game.set_dnf_data(jsoncons::json::parse_string(
    " { \n"
    "   \"car\": { \n"
    "     \"name\": \"Rosberg\", \n"
    "     \"color\": \"blue\" \n"
    "   }, \n"
    "   \"reason\": \"disconnected\" \n"
    " } \n"
  ));

  car& car = _game.get_race().get_car_by_color("blue");

  EXPECT_EQ(true, car.is_disqualified());
  EXPECT_EQ("disconnected", car.get_dnf_reason());
}

TEST_F(game_test, sets_finish)
{
  game_init();

  car& car = _game.get_race().get_car_by_color("red");

  EXPECT_EQ(false, car.is_finished());

  _game.set_finish_data(jsoncons::json::parse_string(
    " { \n"
    "   \"name\": \"Schumacher\", \n"
    "   \"color\": \"red\" \n"
    " } \n"
  ));


  EXPECT_EQ(true, car.is_finished());
}

TEST_F(game_test, sets_turbo)
{
  game_init();

  EXPECT_EQ(0, _game.get_turbo_duration_ticks());
  EXPECT_EQ(0.0, _game.get_turbo_factor());

  _game.set_turbo_available_data(jsoncons::json::parse_string(
    " { \n"
    "   \"turboDurationMilliseconds\": 500.0, \n"
    "   \"turboDurationTicks\": 30, \n"
    "   \"turboFactor\": 3.0 \n"
    " } \n"
  ));

  EXPECT_EQ(30, _game.get_turbo_duration_ticks());
  EXPECT_EQ(3.0, _game.get_turbo_factor());
}

} // namespace

