#include "car.h"
#include <stdexcept>

using namespace std;

car::car(const jsoncons::json& data)
  : _name(data["id"]["name"].as<string>()),
    _color(data["id"]["color"].as<string>()),
    _length(data["dimensions"]["length"].as<double>()),
    _width(data["dimensions"]["width"].as<double>()),
    _guide_flag_position(data["dimensions"]["guideFlagPosition"].as<double>()),
    _angle(0.0),
    _piece_index(-1),
    _in_piece_distance(0.0),
    _start_lane_index(-1),
    _end_lane_index(-1),
    _lap(-1),
    _crashed(false),
    _crash_count(0),
    _lap_times(),
    _ranking_overall(-1),
    _ranking_fastest_lap(-1),
    _dnf_reason(""),
    _finished(false)
{
}

string car::get_name() const
{
  return _name;
}

string car::get_color() const
{
  return _color;
}

double car::get_angle() const
{
  return _angle;
}

int car::get_piece_index() const
{
  return _piece_index;
}

double car::get_in_piece_distance() const
{
  return _in_piece_distance;
}

int car::get_start_lane_index() const
{
  return _start_lane_index;
}

int car::get_end_lane_index() const
{
  return _end_lane_index;
}

int car::get_lap() const
{
  return _lap;
}

bool car::is_crashed() const
{
  return _crashed;
}

int car::get_crash_count() const
{
  return _crash_count;
}

int car::get_lap_time_ticks(int lap) const
{
  int lap_index = lap - 1;
  if (lap_index < 0 || lap_index >= _lap_times.size())
  {
    return -1;
  }
  const tick_milli& lap_time = _lap_times[lap_index];
  return std::get<0>(lap_time);
}

int car::get_lap_time_millis(int lap) const
{
  int lap_index = lap - 1;
  if (lap_index < 0 || lap_index >= _lap_times.size())
  {
    return -1;
  }
  const tick_milli& lap_time = _lap_times[lap_index];
  return std::get<1>(lap_time);
}

bool car::is_disqualified() const
{
  return _dnf_reason.length() > 0;
}

string car::get_dnf_reason() const
{
  return _dnf_reason;
}

bool car::is_finished() const
{
  return _finished;
}

void car::set_position(const jsoncons::json& data)
{
  const jsoncons::json& pos = data["piecePosition"];

  if (_name != data["id"]["name"].as<string>() ||
      _color != data["id"]["color"].as<string>())
  {
    throw std::invalid_argument("data");
  }

  _angle = data["angle"].as<double>();
  _piece_index = pos["pieceIndex"].as<int>();
  _in_piece_distance = pos["inPieceDistance"].as<double>();
  _start_lane_index = pos["lane"]["startLaneIndex"].as<int>();
  _end_lane_index = pos["lane"]["endLaneIndex"].as<int>();
  _lap = pos["lap"].as<int>();
}

void car::set_crashed(bool crashed)
{
  if (crashed != _crashed)
  {
    _crashed = crashed;
    if (crashed)
    {
      ++_crash_count;
    }
  }
}

void car::set_disqualified(bool disqualified, string reason)
{
  if (disqualified)
  {
    _dnf_reason = reason;
  }
  else
  {
    _dnf_reason = "";
  }
}

void car::add_lap(long ticks, long millis)
{
  _lap_times.push_back(std::make_tuple(ticks, millis));
}

void car::update_ranking(int overall, int fastest_lap)
{
  _ranking_overall = overall;
  _ranking_fastest_lap = fastest_lap;
}

void car::set_finished(bool finished)
{
  _finished = finished;
}
