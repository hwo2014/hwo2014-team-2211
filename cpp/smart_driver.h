#include "driver.h"

#define MAX_TRACK_PIECE_LEN  1024

class smart_driver : public driver
{
public:

  smart_driver(game& g);
  virtual ~smart_driver();

  virtual jsoncons::json process_tick(long tick);
  virtual void analyze_track(void);

private:
    double mThrottle_v[MAX_TRACK_PIECE_LEN];
    int    mDesired_lane[MAX_TRACK_PIECE_LEN];
    double slipAng_offset;
    double slipAng_boost;
    double prevPieceDist;
    double prevSpeed;
    unsigned int mTrack_pc_counter;
    unsigned int mTrack_lane_counter;
};

