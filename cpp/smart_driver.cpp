#include "smart_driver.h"
#include "protocol.h"

#define MOV_AVG_SAMPLE_SIZE  1
#define MIN_LANE_IDX         0

driver* create_driver(game& g)
{
  return new smart_driver(g);
}

smart_driver::smart_driver(game& g)
  : driver(g)
{
}

smart_driver::~smart_driver()
{
}

jsoncons::json smart_driver::process_tick(long tick)
{
    double distance = 0.0;
    const track& track = get_game().get_race().get_track();
    const car& my_car = get_game().get_my_car();
    int piece_index = my_car.get_piece_index();
    int end_lane_index  = my_car.get_end_lane_index();
    int curr_lane_index = my_car.get_start_lane_index();
    double piece_distance = my_car.get_in_piece_distance();
    double piece_length;
    double mThrottle = 0.0;
    double mSpeed;
    double mAcc;
    unsigned int next_track_idx;
    int switch_pc_idx;
    
    
    //check for lane switch option in the next piece
    next_track_idx = (piece_index + 1) % mTrack_pc_counter;
    if ( (track.get_pieces())[next_track_idx].is_switch() )
    {
        piece_length = (track.get_pieces())[piece_index].get_length();
        if( (piece_distance + prevSpeed + 8) >= piece_length)
          {
              //should be in Lane 1 , inner lane.
              if( curr_lane_index != mDesired_lane[next_track_idx] )
              {
                  // remove // switch_pc_idx = piece_index;
                  if( curr_lane_index < mDesired_lane[next_track_idx] )
                  {
                      // switch right
                      std::cout << "Ln Switch from lane  " << curr_lane_index
                                << " to inner right lane at pc " << next_track_idx << std::endl;
                      return hwo_protocol::make_switch_right();
                  }
                  else
                  {
                      // switch left
                      std::cout << "Ln Switch from lane  " << curr_lane_index
                                << " to outer  left lane at pc " << next_track_idx << std::endl;
                      return hwo_protocol::make_switch_left();
                  }
              }
    
          }
    }
    
    //calculate moving average for throttle wave shaping.
    for(int itr = 1; itr <= MOV_AVG_SAMPLE_SIZE; itr++)
    {
        next_track_idx = (piece_index + itr) % mTrack_pc_counter ;
        mThrottle     += smart_driver::mThrottle_v[next_track_idx];
    }
    mThrottle = mThrottle / MOV_AVG_SAMPLE_SIZE;
    
    if( std::abs(my_car.get_angle()) >= 5.0 )
    {
        slipAng_offset += 0.2;
        mThrottle = mThrottle - slipAng_offset;
        slipAng_boost = 0.0;
    }
    else
    {
        slipAng_boost += 0.00;
        mThrottle = mThrottle + slipAng_boost;
        slipAng_offset = 0.0;
        /*
        next_track_idx = (piece_index + MOV_AVG_SAMPLE_SIZE) % mTrack_pc_counter ;
        if(mThrottle_v[next_track_idx] > mThrottle)
        {
            std::cout << " calc:" << std::setprecision(3) << mThrottle;
            mThrottle = mThrottle_v[next_track_idx];
        }*/
    }
    
    // calculate speed
    mSpeed = piece_distance - prevPieceDist;
    prevPieceDist = piece_distance;
    //sanity check for speed
    if(mSpeed < 0.0)
    {
        mSpeed = prevSpeed;
    }
    else
    {
    // calculate accelaration (time to increase or decrease speed)
    mAcc   = mSpeed - prevSpeed;
    }
    prevSpeed = mSpeed;
    
    if (mSpeed > 7.50)
    {
        mThrottle = mThrottle - 0.22;
        std::cout << "**" ;
    }
    
    // Sanity check or valid boundary check
    if(mThrottle < 0.0)
    {
        mThrottle = 0.0;
    }
    else if(mThrottle > 1.0)
    {
        mThrottle = 1.0;
    }
    
    std::cout << " Ang:"  << std::setprecision(2) << my_car.get_angle();
    std::cout << " mThr:" << std::setprecision(3) << mThrottle;
    std::cout << " spd:"  << std::setprecision(3) << mSpeed;
    std::cout << " acc:"  << std::setprecision(3) << mAcc << std::endl;
    return hwo_protocol::make_throttle(mThrottle);
}

void smart_driver::analyze_track(void)
{
    const  track& track = get_game().get_race().get_track();
    const  car& my_car  = get_game().get_my_car();
    int    piece_index  = my_car.get_piece_index();
    double distance     = 0.0;
    double total_ang_bt_sw = 0.0;
    double curv_fctr;
    int    switch_idx   = 0;
    int    idx, temp_idx;
    
    std::cout << "# RaceTrack: " << track.get_name() << "  Id:" << track.get_id() << " #" << std::endl;
    
    //calculate look ahead values based on track.
    for (auto itr = track.get_pieces().begin() ; itr != track.get_pieces().end(); ++itr)
    {
        
        if( itr->get_angle() )
        {
            curv_fctr = ( std::abs(itr->get_angle()) / itr->get_length() ) * 0.0175;
            mThrottle_v[mTrack_pc_counter]= (1.00 - (curv_fctr *(std::abs(itr->get_angle()))) );
            
            //sanity check
            if(mThrottle_v[mTrack_pc_counter] < 0.0)
            {
                mThrottle_v[mTrack_pc_counter] = 0.01; // minimum throttle value.
            }
        }
        else
        {
            mThrottle_v[mTrack_pc_counter] = 1.00;
        }
        
        std::cout << std::setprecision(5)
        << "# Pce:" << mTrack_pc_counter
        << " Len:"  << itr->get_length()
        << " Rad:"  << itr->get_radius()
        << " Ang:"  << itr->get_angle()
        << " Swt:"  << itr->is_switch()
        << " Thr:"  << mThrottle_v[mTrack_pc_counter]
        << " Cur:"  << curv_fctr
        << " #"     << std::endl;
        
        mTrack_pc_counter++;
    }
    
    //find maximum lanes for this track
    for (auto itr = track.get_lanes().begin() ; itr != track.get_lanes().end(); ++itr)
    {
        mTrack_lane_counter++;
    }
    
    //calculate desired lane
    temp_idx = 0;
    for (auto itr = track.get_pieces().begin() ; itr != track.get_pieces().end(); ++itr)
    {
        total_ang_bt_sw += itr->get_angle();
        
        if( itr->is_switch() || (temp_idx == (mTrack_pc_counter-1)) )
        {
            if(total_ang_bt_sw < 0.0)
            {
                // should be in Lane 0 or outer lane.
                for(idx = switch_idx; idx <= temp_idx; idx++)
                {
                    mDesired_lane[idx] = MIN_LANE_IDX;
                }
            }
            else
            {
                // should be in Lane 1  or inner lane.
                for(idx = switch_idx; idx <= temp_idx; idx++)
                {
                    mDesired_lane[idx] = mTrack_lane_counter;
                }
            }
            //std::cout << "*" << switch_idx << "*" << total_ang_bt_sw ; // debug ss
            total_ang_bt_sw = 0.0;
            switch_idx = temp_idx;
        }
        temp_idx++;
    }
    
    //for(idx =0; idx < mTrack_pc_counter; idx++)
        //std::cout << "mLane" << mDesired_lane[idx] << " pce" << idx << std::endl; // debug ss

}

