#ifndef HWO_RACE_H
#define HWO_RACE_H

#include <string>
#include <iostream>
#include <jsoncons/json.hpp>
#include "track.h"
#include "track_factory.h"
#include "car.h"
#include "car_factory.h"

class race
{
public:
  race(const jsoncons::json& data, track_factory& track_f, car_factory& car_f);

  int get_laps() const;
  int get_max_lap_time_ms() const;
  bool is_quick_race() const;
  const track& get_track() const;
  const car::car_vector& get_cars() const;
  car& get_car_by_color(std::string color) const;
  long get_millis() const;

  void set_millis(long millis);

private:
  int _laps;
  int _max_lap_time_ms;
  bool _quick_race;
  track& _track;
  car::car_vector& _cars;
  long _millis;
};

#endif
