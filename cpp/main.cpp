#include <iostream>
#include <string>
#include <jsoncons/json.hpp>
#include "protocol.h"
#include "connection.h"
#include "game_logic.h"

using namespace hwo_protocol;

void run(hwo_connection& connection,
         const std::string& name,
         const std::string& key,
         const std::string& trackname,
         int carcount)
{
  game_logic game;
  if (trackname.empty())
  {
    connection.send_requests({ make_join(name, key) });
  }
  else
  {
    connection.send_requests({ make_test_join(name, key, trackname, carcount) });
  }

  for (;;)
  {
    boost::system::error_code error;
    auto response = connection.receive_response(error);

    if (error == boost::asio::error::eof)
    {
      std::cout << "Connection closed" << std::endl;
      break;
    }
    else if (error)
    {
      throw boost::system::system_error(error);
    }

    game.react(response, connection);
  }
}

int main(int argc, const char* argv[])
{
  try
  {
    if (argc < 5 || argc > 7)
    {
      std::cerr << "Usage: ./run host port botname botkey [trackname] [carcount]" << std::endl;
      return 1;
    }

    const std::string host(argv[1]);
    const std::string port(argv[2]);
    const std::string name(argv[3]);
    const std::string key(argv[4]);
    std::string trackname = "";
    int carcount = 0;
    std::cout << "Host: " << host << ", port: " << port << ", name: " << name << ", key:" << key << std::endl;

    if (argc >= 6)
    {
      trackname = argv[5];
      carcount = (argc == 7 ? atoi(argv[6]) : 1);
      std::cout << "Track: " << trackname << ", Car Count: " << carcount << std::endl;
    }

    hwo_connection connection(host, port);
    run(connection, name, key, trackname, carcount);
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
    return 2;
  }

  return 0;
}
