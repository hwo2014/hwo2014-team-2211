#include <cmath>
#include "gtest/gtest.h"
#include "game.h"
#include "drift_driver.h"

namespace {

class drift_driver_test : public ::testing::Test {
protected:
  drift_driver_test()
    : _t_p_factory(),
      _t_l_factory(),
      _t_factory(_t_p_factory, _t_l_factory),
      _c_factory(),
      _r_factory(_t_factory, _c_factory),
      _game(_r_factory),
      _driver(_game)
  {
  }

  virtual ~drift_driver_test()
  {
  }

  void game_init();

  track_piece_factory _t_p_factory;
  track_lane_factory _t_l_factory;
  track_factory _t_factory;
  car_factory _c_factory;
  race_factory _r_factory;
  game _game;
  drift_driver _driver;
};

void drift_driver_test::game_init()
{
  _game.set_game_init_data(jsoncons::json::parse_string(
    " { \n"
    " \"race\": { \n"
    "   \"track\": { \n"
    "     \"id\": \"indianapolis\", \n"
    "     \"name\": \"Indianapolis\", \n"
    "     \"pieces\": [ \n"
    "       { \n"
    "         \"length\": 100.0 \n"
    "       }, \n"
    "       { \n"
    "         \"length\": 100.0, \n"
    "         \"switch\": true \n"
    "       }, \n"
    "       { \n"
    "         \"radius\": 150, \n"
    "         \"angle\": 90 \n"
    "       }, \n"
    "       { \n"
    "         \"length\": 50.0 \n"
    "       }, \n"
    "       { \n"
    "         \"radius\": 100, \n"
    "         \"angle\": 45 \n"
    "       }, \n"
    "       { \n"
    "         \"length\": 75.0 \n"
    "       } \n"
    "     ], \n"
    "     \"lanes\": [ \n"
    "       { \n"
    "         \"distanceFromCenter\": -20, \n"
    "         \"index\": 0 \n"
    "       }, \n"
    "       { \n"
    "         \"distanceFromCenter\": 0, \n"
    "         \"index\": 1 \n"
    "       }, \n"
    "       { \n"
    "         \"distanceFromCenter\": 20, \n"
    "         \"index\": 2 \n"
    "       } \n"
    "     ], \n"
    "     \"startingPoint\": { \n"
    "       \"position\": { \n"
    "         \"x\": -340.0, \n"
    "         \"y\": -96.0 \n"
    "       }, \n"
    "       \"angle\": 90.0 \n"
    "     } \n"
    "   }, \n"
    "   \"cars\": [ \n"
    "     { \n"
    "       \"id\": { \n"
    "         \"name\": \"Schumacher\", \n"
    "         \"color\": \"red\" \n"
    "       }, \n"
    "       \"dimensions\": { \n"
    "         \"length\": 40.0, \n"
    "         \"width\": 20.0, \n"
    "         \"guideFlagPosition\": 10.0 \n"
    "       } \n"
    "     }, \n"
    "     { \n"
    "       \"id\": { \n"
    "         \"name\": \"Rosberg\", \n"
    "         \"color\": \"blue\" \n"
    "       }, \n"
    "       \"dimensions\": { \n"
    "         \"length\": 40.0, \n"
    "         \"width\": 20.0, \n"
    "         \"guideFlagPosition\": 10.0 \n"
    "       } \n"
    "     } \n"
    "   ], \n"
    "   \"raceSession\": { \n"
    "     \"laps\": 3, \n"
    "     \"maxLapTimeMs\": 30000, \n"
    "     \"quickRace\": true \n"
    "   } \n"
    " }} \n"
  ));
}

TEST_F(drift_driver_test, can_get_max_throttle)
{
  game_init();

  double last_value = MAX_THROTTLE;
  double value = MAX_THROTTLE;

  EXPECT_DOUBLE_EQ(MAX_THROTTLE, _driver.get_max_throttle(0));
  EXPECT_DOUBLE_EQ(MAX_THROTTLE, _driver.get_max_throttle(MAX_SPEED_RADIUS));

  for (double radius = MAX_SPEED_RADIUS - 1; radius > 0.0; radius -= 0.10)
  {
    last_value = value;
    value = _driver.get_max_throttle(radius);
    EXPECT_LT(value, last_value);
  }
}

TEST_F(drift_driver_test, can_calculate_desired_throttle)
{
  game_init();

  const track& track = _game.get_race().get_track();

  // Test full throttle within the same piece.
  EXPECT_EQ(1.0, _driver.calculate_desired_throttle(track, 0, 25, 50));

  // Test full throttle between 2 pieces.
  EXPECT_EQ(1.0, _driver.calculate_desired_throttle(track, 0, 50, 75));

  // Test full throttle within the same piece.
  EXPECT_DOUBLE_EQ(150, _game.get_race().get_track().get_pieces()[2].get_radius());
  EXPECT_TRUE(1.0 > _driver.calculate_desired_throttle(track, 2, 0, 0));

  // Test partial throttle between 2 pieces.
  EXPECT_TRUE(1.0 > _driver.calculate_desired_throttle(track, 1, 50, 75));
}


} // namespace

