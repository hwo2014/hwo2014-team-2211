#include "driver.h"
#include "protocol.h"

class default_driver : public driver
{
public:
  default_driver(game& g);
  virtual ~default_driver();

  virtual jsoncons::json process_tick(long tick);

private:
};

driver* create_driver(game& g)
{
  return new default_driver(g);
}

default_driver::default_driver(game& g)
  : driver(g)
{
}

default_driver::~default_driver()
{
}

jsoncons::json default_driver::process_tick(long tick)
{
  return hwo_protocol::make_throttle(0.64);
}

